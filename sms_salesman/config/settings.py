from pathlib import Path
from decouple import config, Csv

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
SECRET_KEY = config('SECRET_KEY')
DEBUG = config('DEBUG', default=False, cast=bool)
LOGS_DIR = Path(BASE_DIR, '../logs')


# mtt
MTT_ACCOUNT_SID = config('MTT_ACCOUNT_SID')
MTT_ACCOUNT_TOKEN = config('MTT_ACCOUNT_TOKEN')
MTT_URL = config('MTT_URL')


# smshub
MAX_ACTIVE_SMS_POOL = config('MAX_ACTIVE_SMS_POOL', default=0, cast=int)
ZERO_AVAILABLE_NUMBERS = config('ZERO_AVAILABLE_NUMBERS', default=True, cast=bool)

ALLOWED_HOSTS = config('ALLOWED_HOSTS', default='', cast=Csv())
CSRF_TRUSTED_ORIGINS = config('CSRF_TRUSTED_ORIGINS', default='', cast=Csv())

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    
    'accounts',
    'mtt',
    'sms_hub'
]

AUTH_USER_MODEL = 'accounts.User'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler'
        },
        'django': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': Path(LOGS_DIR, 'django.log'),
            'formatter': 'verbose'
        },
        'send-sms': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': Path(LOGS_DIR, 'send_sms.log'),
            'when': 'M',
            'backupCount': 1,
            'formatter': 'verbose'
        },
        'update-sms': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': Path(LOGS_DIR, 'update_sms.log'),
            'when': 'M',
            'backupCount': 1,
            'formatter': 'verbose'
        },
        'smshub': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': Path(LOGS_DIR, 'smshub.log'),
            'formatter': 'verbose'
        },
        'send_numbers_v2': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': Path(LOGS_DIR, 'send_numbers_v2.log'),
            'formatter': 'verbose'
        },
    },

    'loggers': {
        'send-sms': {
            'level': 'INFO',
            'handlers': ['send-sms'],
            'formatter': 'verbose'
        },
        'update-sms': {
            'level': 'INFO',
            'handlers': ['update-sms'],
            'formatter': 'verbose'
        },
        'smshub': {
            'level': 'INFO',
            'handlers': ['smshub'] if not DEBUG else ['console', 'django'],
            'formatter': 'verbose'
        },
        'django': {
            'level': 'INFO',
            'handlers': ['django'] if not DEBUG else ['console', 'django'],
            'formatter': 'verbose'
        },
        'send_numbers_v2': {
            'level': 'INFO',
            'handlers': ['send_numbers_v2'] if not DEBUG else ['console', 'django'],
            'formatter': 'verbose'
        }
    },
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {module} {message}',
            'style': '{',
        },
    },
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': config('DB_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PASSWORD'),
        'HOST': config('DB_HOST', default='127.0.0.1'),
        'PORT': config('DB_PORT', default=5432, cast=int),
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = False

STATIC_URL = 'static/'
STATIC_ROOT = config('STATIC_ROOT', Path(BASE_DIR, '/static/'))

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
