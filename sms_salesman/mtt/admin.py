from django.contrib import admin
from .models import TelNumber, Sms


@admin.register(TelNumber)
class TelNumberAdmin(admin.ModelAdmin):
    list_display = ('tel_number', )


@admin.register(Sms)
class SmsAdmin(admin.ModelAdmin):
    list_display = ('alfa', 'tel_number', 'created_at')

