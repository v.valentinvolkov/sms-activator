from django.apps import AppConfig


class MttConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mtt'
