import logging
import time
import timeit
from datetime import datetime, timedelta

import requests
from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import IntegrityError

from utils.activation_utils import get_active_activations
from utils.sms_utils import create_sms_from_mtt


logger = logging.getLogger('update-sms')


class Command(BaseCommand):
    help = 'Update sms active numbers'

    def add_arguments(self, parser):
        parser.add_argument('--interval', type=int, default=5, help='Interval between calling (sec)')
        parser.add_argument('--once', action='store_true', help='Call command only once')

    accountSID = settings.MTT_ACCOUNT_SID
    token = settings.MTT_ACCOUNT_TOKEN
    mtt_url = settings.MTT_URL

    url = f'{mtt_url}/v2/accounts/{accountSID}/messages'

    new_sms = 0

    def update_sms(self, *args, **options):
        """ Get sms for each active activation. """
        active_activations = get_active_activations()
        # already_requested_numbers = []
        for activation in active_activations:
            tel_number = activation.tel_number
            limit = 20  # default, max
            request_params = {'number': tel_number.tel_number, 'direction': 'inbound', 'offset': 0}
            # loop through all pages of sms for one number
            while True:
                # TODO: mock get method
                # TODO: stop when rich old sms in mtt
                response = requests.get(self.url, auth=(self.accountSID, self.token), params=request_params)
                messages = response.json()['messages'] or []
                logger.info(f'{tel_number.tel_number}: total: {len(messages)}')
                # loop through all sms on one page
                for sms in messages:
                    try:
                        create_sms_from_mtt(tel_number=tel_number, mtt_data=sms)
                        self.new_sms += 1
                    except IntegrityError:
                        print('IntegrityError')
                        break
                    except Exception as e:
                        logger.error(f'Error sms creating {tel_number.tel_number}: {str(e)}')
                else:
                    # if page doesn't have existed sms check next page
                    if response.json()['next'] == '':
                        # if next link is None - break pagination
                        break
                    else:
                        request_params['offset'] += limit
                        continue
                # if "break" was while loop though page - break loop through pages
                break

    def handle(self, *args, **options):
        while True:
            start = datetime.now()
            try:
                self.update_sms()
            except Exception as e:
                print(e)
            stop = datetime.now()
            logger.info(f'Total sms: {self.new_sms}, Sending time: {stop-start}')
            if options['once']:
                print('One call finished')
                break

            time.sleep(options.get('interval'))

