import csv
import sys
from datetime import timedelta

from django.core.management.base import BaseCommand

from accounts.models import Account, AccountService, AccountSms
from mtt.models import Sms, TelNumber
from sms_hub.models import Service


class Command(BaseCommand):
    help = 'Load sms from scv -\n' \
           '"message_sid; alfa; text; tel_number; created_at; account_0; account_1"\n' \
           '"account_*": bool'

    def add_arguments(self, parser):
        parser.add_argument('-f', type=str, help='CSV file with sms', required=True)

    def handle(self, *args, **options):
        csv.register_dialect('dialect', delimiter=';')
        filename = options.get('f')

        new_sms = 0
        new_account_sms = 0
        with open(filename, newline='') as f:
            reader = csv.DictReader(f, dialect='dialect')
            # clean sms
            smses = Sms.objects.all()
            print(f'{smses.count()} sms to remove')
            smses.delete()

            try:
                for row in reader:
                    # create sms
                    tel_number = TelNumber.objects.get(tel_number=row['tel_number'])
                    sms = Sms.objects.create(message_sid=row['message_sid'],
                                             alfa=row['alfa'],
                                             tel_number=tel_number,
                                             sms_text=row['text'],
                                             created_at=row['created_at'])
                    new_sms += 1
                    print(f'Create sms {sms}')
                    # # create account sms
                    # for account_name, is_success in list(row.items())[5:]:
                    #     if is_success == 'True':
                    #         account = Account.objects.get(account_name)
                    #         account_sms = AccountSms.objects.create(
                    #             account=account,
                    #             sms=sms,
                    #             is_success=True)
                    #         new_account_sms += 1

            except csv.Error as e:
                sys.exit('file {}, line {}: {}'.format(filename, reader.line_num, e))
        print(f'new_sms: {new_sms}\n'
              f'new_account_sms: {new_account_sms}\n')
