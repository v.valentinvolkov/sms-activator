import requests
from django.core.management.base import BaseCommand
from django.conf import settings

from ...models import TelNumber


class Command(BaseCommand):
    help = 'Update all numbers from mtt to database'

    accountSID = settings.MTT_ACCOUNT_SID
    token = settings.MTT_ACCOUNT_TOKEN
    mtt_url = settings.MTT_URL

    url = f'{mtt_url}/v2/accounts/{accountSID}/numbers'

    def handle(self, *args, **options):
        # remove all numbers
        count = TelNumber.objects.count()
        print(f'{count} removed numbers')
        TelNumber.objects.all().delete()
        # get mtt numbers
        limit = 20  # default, max
        params = {'offset': 0}
        while True:
            response = requests.get(self.url, auth=(self.accountSID, self.token), params=params)
            # save numbers
            for number in response.json()['numbers']:
                TelNumber.objects.create(tel_number=number['number'])
                print(number['number'])
            if response.json()['next'] == '':
                break
            else:
                params['offset'] += limit
        count = TelNumber.objects.count()
        print(f'Set {count} numbers')
