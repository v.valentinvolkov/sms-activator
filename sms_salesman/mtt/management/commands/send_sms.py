import logging
import time
from datetime import datetime
from typing import Optional

import requests
from django.core.management.base import BaseCommand

from accounts.models import AccountSms, Account
from mtt.models import Sms

logger = logging.getLogger('send-sms')


def get_sms_sending_data(account: Account, sms: Sms) -> Optional[dict]:
    """ Get sms data to send in format. """
    if account.name == 'smshub' or account.name == 'onlinesim':
        return {
            "smsId": sms.message_sid,
            "phoneFrom": str(sms.alfa),
            "phone": str(sms.tel_number.tel_number),
            "text": sms.sms_text,
            "action": "PUSH_SMS",
            "key": account.key
        }

    elif account.name == 'onlinesim_v2':
        return {
            "from": str(sms.alfa),
            "number": str(sms.tel_number.tel_number),
            "message": sms.sms_text,
            "date": str(sms.created_at.timestamp())
        }
    return None


def send_request(account: Account, data: dict) -> bool:
    """ Send sms with data"""
    response = None
    if account.name == 'smshub' or account.name == 'onlinesim':
        response = requests.post(account.sms_endpoint, json=data)
        if response.json().get('status', '') == 'SUCCESS':
            return True
    elif account.name == 'onlinesim_v2':
        response = requests.post(account.sms_endpoint, json=data,
                                 headers={"Authorization": '9cf6d8b693613a058ca0b86dd4724b49'})
        if response.json().get('response', 0) == '1':
            return True
    logger.warning(f'Send sms fail {account.name}: status - {response.status_code}\n'
                   f'response - {response.json()} ')
    return False


def send_sms():
    """ Send sms to accounts' sms_endpoint if account is current_account in some active activation. """
    # get all active account_sms
    active_account_sms = AccountSms.objects.filter(is_success=False)
    logger.info(f'Account sms to send: {len(active_account_sms)}')
    for account_sms in active_account_sms:
        sms = account_sms.sms
        account = account_sms.account
        try:
            data = get_sms_sending_data(account=account, sms=sms)
            if data is None:
                logger.error(f'Cannot find sms data format for account {account.name}')
                continue
            response_success = send_request(account=account, data=data)
            if response_success:
                account_sms.is_success = True
                account_sms.save()
        except Exception as e:
            logger.error(e)
            continue


class Command(BaseCommand):
    help = 'Send to smshub and update sms with is_success False'

    def add_arguments(self, parser):
        parser.add_argument('--interval', type=int, default=5, help='Interval between calling (sec)')
        parser.add_argument('--once', action='store_true', help='Call command only once')

    def handle(self, *args, **options):
        while True:
            start = datetime.now()
            try:
                send_sms()
            except Exception as e:
                logger.error(e)
            stop = datetime.now()
            logger.info(f'Sending time: {stop-start}')
            if options['once']:
                print('One call finished')
                break

            time.sleep(options.get('interval'))
