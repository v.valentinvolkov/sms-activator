from django.db import models


class TelNumber(models.Model):
    tel_number = models.CharField('Номер телефона', unique=True, max_length=20)

    def __str__(self):
        return self.tel_number


class Sms(models.Model):
    message_sid = models.CharField('sid mtt', max_length=50, unique=True)
    alfa = models.CharField('Внешний номер', max_length=20)
    tel_number = models.ForeignKey('mtt.TelNumber', on_delete=models.CASCADE)
    sms_text = models.CharField('Текст смс', max_length=255)
    created_at = models.DateTimeField('Время получения', null=True)

    def __str__(self):
        return f'{self.alfa} -> {self.tel_number}'
