from datetime import datetime

from django.db.models import Exists, OuterRef

from accounts.models import AccountSms, Account
from mtt.models import Sms, TelNumber
from sms_hub.models import Activation


def get_all_active_sms():
    """ Return all not successfully sent sms"""
    return Sms.objects.filter(Exists(AccountSms.objects.filter(sms=OuterRef('pk'), is_success=False)))


def create_sms_from_mtt(tel_number: TelNumber, mtt_data: dict) -> Sms:
    """ Create sms got for activation with data from mtt. """
    # create sms
    sms = Sms.objects.create(
        message_sid=mtt_data.get('message_sid'),
        alfa=mtt_data.get('from'),
        sms_text=mtt_data.get('text'),
        created_at=datetime.fromtimestamp(mtt_data.get('created_at')),
        tel_number=tel_number
    )

    # create account_sms
    accounts_to_send_sms = Account.objects.filter(
        Exists(Activation.objects.filter(tel_number=tel_number, current_account=OuterRef('pk')))
    )
    for account in accounts_to_send_sms:
        AccountSms.objects.create(sms=sms, account=account)
    return sms
