from accounts.models import Account, AccountService
from sms_hub.models import Service


def get_account_service_by_code(account: Account, service_code: str) -> AccountService:
    return AccountService.objects.get(account=account, service_code=service_code)
