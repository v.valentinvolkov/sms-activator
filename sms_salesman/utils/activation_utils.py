from typing import Optional

from django.db.models import F, QuerySet, Q

from accounts.models import AccountActivation, Account, AccountService
from mtt.models import TelNumber
from sms_hub.models import Activation, Service


def init_activation(account_service: AccountService, tel_number: TelNumber) -> (Activation, AccountActivation):
    """ Create or update activation and account_activation for tel_number-service. """
    account = account_service.account
    service = account_service.service

    if not Activation.objects.filter(tel_number=tel_number, service=service).exists():
        activation = Activation.objects.create(
            tel_number=tel_number,
            service=service,
            status=Activation.STATUS.active,
            current_account=account
        )
    elif not Activation.objects.filter(tel_number=tel_number, service=service, status=Activation.STATUS.active).exists():
        activation_updating = Activation.objects.filter(
            Q(tel_number=tel_number) &
            Q(service=service) &
            ~Q(status=Activation.STATUS.active)).update(status=Activation.STATUS.active, current_account=account)
        if not activation_updating:
            raise Exception("Concurrent initialization of activation")
        activation = Activation.objects.get(tel_number=tel_number, service=service)
    else:
        raise Exception("Concurrent initialization of activation")

    if AccountActivation.objects.filter(activation=activation, account=account).exists():
        account_activation = AccountActivation.objects.get(activation=activation, account=account)
        account_activation.tries = F('tries') + 1
    else:
        account_activation = AccountActivation.objects.create(activation=activation, account=account, tries=1)
    return activation, account_activation


def try_init_activation(account_service: AccountService, tel_numbers: QuerySet) -> Optional[Activation]:
    """ Try init activation for one tel_number or raise "NO_NUMBERS". """
    for tel_number in tel_numbers:
        try:
            activation, account_activation = init_activation(account_service=account_service, tel_number=tel_number)
            return activation
        except Exception as e:
            print(e)
            continue
    return None


def finish_activation(account: Account, activation_id: int, status: int) -> (Activation, Optional[AccountActivation]):
    """ Finish activation for account. """
    activation = Activation.objects.get(pk=activation_id, current_account=account)
    if activation.status != status:
        activation.status = status
        activation.save()
        account_activation = AccountActivation.objects.get(activation=activation, account=account)
        account_activation.tries = F('tries') + 1
        account_activation.save()
        return activation, account_activation
    return activation, None


def get_active_activations():
    """ Get all active activations distinct by tel_numbers. """
    active_activations = Activation.objects.filter(status=Activation.STATUS.active).distinct('tel_number')
    return active_activations

