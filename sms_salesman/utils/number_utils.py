import datetime
from typing import Optional, Union, Iterable

from django.db.models import Q, Exists, OuterRef, Subquery, QuerySet

from accounts.models import Account, AccountService, AccountActivation
from sms_hub.models import Service, Activation
from mtt.models import TelNumber


def _get_available_numbers_count(account_service: AccountService, tel_numbers_count: int) -> int:
    """ Get all not available numbers for account-service with account_service.limit. """
    account = account_service.account
    service = account_service.service

    activations = Activation.objects.filter(service=service)

    # check limit
    finished_active_current_account_activations = activations.filter(Q(current_account=account) & ~Q(status=4))
    if finished_active_current_account_activations.count() >= account_service.limit:
        return 0

    # filter by this QuerySet give numbers dependent of accounts' tries - repeated activations
    account_activation = AccountActivation.objects.filter(account=account,
                                                          activation=OuterRef('pk'))

    # datetime after which number not valid
    allowed_try_datetime = datetime.datetime.now() - account_service.try_interval
    allowed_tries = account_service.allowed_tries

    not_retry_activations = account_activation.filter(account=account).filter(
        Q(tries__gte=allowed_tries) |
        Q(last_try_datetime__gte=allowed_try_datetime)
    )

    finished_active_not_retry_activations = activations.filter(
        ~Q(status=4) |
        Exists(not_retry_activations)
    )

    available_numbers_count = tel_numbers_count - finished_active_not_retry_activations.count()
    return available_numbers_count


def _get_available_all_numbers(account_service: AccountService, tel_numbers: QuerySet = None) -> QuerySet:
    """ Get available numbers for service-account. """
    account = account_service.account
    service = account_service.service
    # if tel_numbers QuerySet is None - get available numbers among all numbers
    if tel_numbers is None:
        tel_numbers = TelNumber.objects.all()

    # filter by this QuerySet give numbers independent of accounts - finish or active activations
    activation = Activation.objects.filter(service=service, tel_number=OuterRef(OuterRef('pk')))
    finished_or_active_activation = activation.filter(~Q(status=4))

    # datetime after which number not valid
    allowed_try_datetime = datetime.datetime.now() - account_service.try_interval
    allowed_tries = account_service.allowed_tries

    # filter by this QuerySet give numbers dependent of accounts' tries - repeated activations
    account_activation = AccountActivation.objects.filter(account=account,
                                                          activation=Subquery(activation.values('pk')))
    not_retry_activation = account_activation.filter(
        Q(tries__gte=allowed_tries) |
        Q(last_try_datetime__gte=allowed_try_datetime)
    )

    available_numbers = tel_numbers.filter(
        ~Exists(finished_or_active_activation) &  # number don't have finished activation with this service
        ~Exists(not_retry_activation)   # number don't have account activation isn't valid to retry
    )
    return available_numbers


def _get_available_numbers_for_v2(account_service: AccountService,
                                  tel_numbers: QuerySet = None,
                                  exclude_accounts: QuerySet[Account] = None):
    """ Get numbers exclude active or finished activations from exclude_accounts - available for v2. """
    account = account_service.account
    service = account_service.service
    # if tel_numbers QuerySet is None - get available numbers among all numbers
    if tel_numbers is None:
        tel_numbers = TelNumber.objects.all()
    if exclude_accounts is not None and exclude_accounts.exists():
        # filter by this QuerySet give numbers independent of accounts - finish or active activations
        finished_active_activation = Activation.objects.filter(
            Q(service=service),
            Q(tel_number=OuterRef('pk')),
            ~Q(status=4)
        )
        # get finished or active activation for exclude accounts only (to exclude this activations in tel_number filter)
        for exclude_account in exclude_accounts:
            finished_active_activation = finished_active_activation.filter(current_account=exclude_account)

        available_numbers_v2 = tel_numbers.filter(~Exists(finished_active_activation))
    else:
        available_numbers_v2 = tel_numbers
    return available_numbers_v2


def _exclude_prefixes(numbers: QuerySet, exclude_prefixes: Iterable[str]) -> QuerySet:
    """ Exclude number with exclude_prefixes. """
    # get Q stmt for filter exclude prefixes
    start_with_exclude_prefixes_stmt = Q()
    for prefix in exclude_prefixes:
        start_with_exclude_prefixes_stmt |= Q(tel_number__istartswith=prefix)
    return numbers.filter(~start_with_exclude_prefixes_stmt)


def _get_available_numbers(account_service: AccountService,
                           exclude_prefixes: Iterable[str] = ()) -> QuerySet:
    """ Get numbers for account-service by priority and limit. """
    available_numbers = _get_available_all_numbers(account_service=account_service)
    if exclude_prefixes:
        available_numbers = _exclude_prefixes(numbers=available_numbers, exclude_prefixes=exclude_prefixes)
    return available_numbers


def _get_available_numbers_with_mode(account_service: AccountService, exclude_prefixes: Iterable[str] = ()) -> QuerySet:
    """ Get available numbers for service-account according priority. """
    mode = account_service.service.mode
    if mode == Service.MODES.full:
        return _get_available_numbers(account_service=account_service, exclude_prefixes=exclude_prefixes)
    elif mode == Service.MODES.limit:
        return _get_available_numbers(account_service=account_service,
                                      exclude_prefixes=exclude_prefixes,
                                      limit_affect_on_available_numbers=True)
    elif mode == Service.MODES.priority:
        return _get_available_numbers(account_service=account_service,
                                      priority_exclude=True,
                                      exclude_prefixes=exclude_prefixes)
    elif mode == Service.MODES.limit_priority:
        return _get_available_numbers(account_service=account_service,
                                      priority_exclude=True,
                                      exclude_prefixes=exclude_prefixes,
                                      limit_affect_on_available_numbers=True)
    else:
        # any unknown mode return full
        return _get_available_numbers(account_service=account_service, exclude_prefixes=exclude_prefixes)


def get_service_map(account: Account, zero_available_numbers: bool = True) -> dict:
    """ Get service (service_code) map for account. """
    account_services = AccountService.objects.filter(account=account) or []  # if account don't have any account_service
    service_map = {}
    all_numbers_count = TelNumber.objects.count()
    for account_service in account_services:
        if zero_available_numbers or not account_service.is_active or not account_service.service.is_active:
            service_map[account_service.service_code] = 0
        else:
            available_numbers_count = _get_available_numbers_count(account_service=account_service,
                                                                   tel_numbers_count=all_numbers_count)
            service_map[account_service.service_code] = available_numbers_count
    return service_map


def get_number(account_service: AccountService, exclude_prefixes: list[str]) -> QuerySet:
    """ Get available number for account and service exclude number with exclude_prefixes. """
    # take 20 numbers for try init activation
    # if {"status":"ERROR","error":"Concurrent error - too many request/sec"} - need increase limit
    return _get_available_numbers_with_mode(account_service=account_service, exclude_prefixes=exclude_prefixes)[:100]


def get_available_numbers_between_services(account_services: QuerySet,
                                           tel_numbers: QuerySet = None,
                                           exclude_accounts: QuerySet[Account] = None) -> QuerySet:
    """ Get numbers that are available for v2 with all services. """
    for account_service in account_services:
        tel_numbers = _get_available_numbers_for_v2(account_service=account_service,
                                                    tel_numbers=tel_numbers,
                                                    exclude_accounts=exclude_accounts)
    return tel_numbers
