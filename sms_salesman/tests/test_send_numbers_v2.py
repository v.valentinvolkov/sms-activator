from django.test import TestCase

from tests.fixture import set_test_data, create_act_acc_ser, gen_message_data_from_mtt
from sms_hub.management.commands.send_numbers_to_onlinesim_v2 import get_numbers_for_online_v2, send_numbers


class SendNumbersV2Tests(TestCase):

    @classmethod
    def setUpTestData(cls):
        # set up base data (manually setting data to start)
        set_test_data(cls=cls)

    def test_get_numbers_for_online_v2(self):
        """test: get available numbers (list[str]) with limit"""
        numbers = get_numbers_for_online_v2(account=self.accounts[0], limit=3)
        self.assertEqual(len(numbers), 3)
        self.assertEqual(numbers[0], '70000000010')
        self.assertEqual(numbers[1], '70000000011')
        self.assertEqual(numbers[2], '70000000012')
