import datetime
from unittest import TestCase

from accounts.models import AccountService, Account, AccountActivation
from mtt.models import TelNumber, Sms
from sms_hub.models import Service, Activation


def set_test_data(cls: type(TestCase)):
    cls.numbers = []
    for i in range(10, 30):
        cls.numbers.append(
            TelNumber.objects.create(tel_number=f'700000000{i}'))

    cls.services = []
    for i in range(5):
        cls.services.append(
            Service.objects.create(name=f'service_{i}')
        )

    cls.accounts = []
    for i in range(3):
        cls.accounts.append(Account.objects.create(
            name=f'acc_{i}',
            key=f'key_{i}',
            sms_endpoint=f'sms_endpoint_{i}'
        ))

    cls.account_services = []
    for service in cls.services[:3]:
        for acc in cls.accounts:
            cls.account_services.append(
                AccountService.objects.create(
                    account=acc,
                    service=service,
                    service_code=f'{acc.name}/{service.name}',
                    allowed_tries=3,
                    try_interval=datetime.timedelta(seconds=5),
                    priority=cls.accounts.index(acc)
                )
            )
    for acc in cls.accounts[:2]:
        cls.account_services.append(
            AccountService.objects.create(
                account=acc,
                service=cls.services[3],
                service_code=f'{acc.name}/{cls.services[3].name}',
                allowed_tries=3,
                try_interval=datetime.timedelta(seconds=5),
                priority=cls.accounts.index(acc)
            )
        )
    cls.account_services.append(
        AccountService.objects.create(
            account=cls.accounts[0],
            service=cls.services[4],
            service_code=f'{cls.accounts[0].name}/{cls.services[4].name}',
            allowed_tries=3,
            try_interval=datetime.timedelta(seconds=5),
            priority=cls.accounts.index(cls.accounts[0])
        )
    )
    return cls


def get_account_service(account_id: int, service_id: int) -> (Account, Service, AccountService):
    account = Account.objects.all()[account_id]
    service = Service.objects.all()[service_id]
    account_service = AccountService.objects.get(account=account, service=service)
    return account, service, account_service


def create_act_acc_ser(account: Account,
                       service: Service,
                       number: TelNumber,
                       status: int = 3,
                       tries: int = 1) -> (Activation, AccountActivation):
    """ Create activation with account and service"""
    activation = Activation.objects.create(service=service,
                                           tel_number=number,
                                           status=status,
                                           current_account=account)
    account_activation = AccountActivation.objects.create(account=account, activation=activation, tries=tries)
    return activation, account_activation


def gen_message_data_from_mtt(num: int, tel_number: TelNumber, alfa: str = 'alfa'):
    """ return generator for making messages"""
    message_template = {
        "account_sid": "1",
        "message_sid": "1",
        "text": "Text sms",
        "created_at": 1649687930,
        "direction": "inbound",
        "number": tel_number.tel_number,
        "external_number": "alfa",
        "from": alfa,
        "to": tel_number.tel_number,
        "h323_conf_id": "***",
        "status": "received",
        "segment_count": 1,
        "channel": "sms"
    }
    sms_count = Sms.objects.filter(tel_number=tel_number).count()
    messages = []
    for i in range(num):
        message = message_template
        message['account_sid'] = message['message_sid'] = f'{tel_number.tel_number}-{sms_count}'
        messages.append(message)
    return messages
