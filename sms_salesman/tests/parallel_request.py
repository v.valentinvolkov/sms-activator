import threading
import requests
import time


def get_number(url):
    res = requests.post(url, json={
        "action": "GET_NUMBER",
        "key": "1762U88aa199db7c0f5d5c161ffa4d16704ae",
        "country": "russia",
        "operator": "mtt_virtual",
        "service": "ft",
        "sum": "10"
    })
    print(f'{url}: {res.text}')


def go():

    start = time.time()
    for i in range(0):
        get_number('http://127.0.0.1:8000/sms_hub')
    print(f'Sequential: {time.time() - start : .2f} seconds')

    print()

    start = time.time()
    threads = []
    for i in range(30):
        thread = threading.Thread(target=get_number, args=('http://127.0.0.1:8000/sms_hub',))
        threads.append(thread)
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()

    print(f'Threading: {time.time() - start : .2f} seconds')


if __name__ == "__main__":
    go()
