from typing import Iterable

from django.conf import settings
from django.test import TestCase
from django.test import Client

from accounts.models import Account, AccountService, AccountActivation
from sms_hub.models import Service
from tests import fixture
from tests.fixture import create_act_acc_ser

GET_SERVICES = {
    'action': 'GET_SERVICES',
    'key': ''
}

GET_NUMBER = {
    'action': 'GET_NUMBER',
    'key': '',
    'country': 'russia',
    'service': '',
    'operator': '',
    'sum': '',
    'exceptionPhoneSet': ''
}

FINISH_ACTIVATION = {
    'action': 'FINISH_ACTIVATION',
    'key': '',
    'activationId': None,
    'status': None,
}


def _query_account_service_maps(client: Client, accounts: Iterable) -> dict:
    """ Make request to GET_SERVICES and return one dict with service maps for accounts. """
    account_service_maps = {}
    for account in accounts:
        GET_SERVICES['key'] = account.key
        response = client.post('/sms_hub', data=GET_SERVICES)
        operator = response.json()['countryList'][0]['operatorMap']
        service_map = operator[account.operator]
        account_service_maps[account.name] = service_map
    return account_service_maps


def _query_numbers(client: Client, account: Account, service_code: str) -> (int, int):
    """Make request to GET_NUMBER and return number and activationId"""
    GET_NUMBER['key'] = account.key
    GET_NUMBER['service'] = service_code
    response = client.post('/sms_hub', data=GET_NUMBER)
    return response.json()['number'], response.json()['activationId']


def _query_finish_activation(client: Client, account: Account, activation_id: int, status: int) -> None:
    """ Make request to FINISH_ACTIVATION and return"""
    FINISH_ACTIVATION['key'] = account.key
    FINISH_ACTIVATION['activationId'] = activation_id
    FINISH_ACTIVATION['status'] = status
    return client.post('/sms_hub', data=FINISH_ACTIVATION)


class ActionTests(TestCase):
    """ Functional tests of actions (action's names in title of methods. """
    @classmethod
    def setUpTestData(cls):
        # set up base data (manually setting data to start)
        fixture.set_test_data(cls=cls)
        AccountService.objects.all().update(limit=20)
        cls.client = Client()

    def test_get_services(self):
        """test: request get services only"""
        # GET_SERVICES
        all_service_maps = _query_account_service_maps(client=self.client, accounts=self.accounts)
        # account 0
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_0'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_1'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_2'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_3'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_4'], 20)
        # account 1
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_0'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_1'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_2'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_3'], 20)
        self.assertNotIn('acc_1/service_4', all_service_maps['acc_1'],)
        # account 2
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_0'], 20)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_1'], 20)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_2'], 20)
        self.assertNotIn('acc_2/service_3', all_service_maps['acc_2'], )
        self.assertNotIn('acc_2/service_4', all_service_maps['acc_2'], )
    
    def test_get_services_with_get_number(self):
        """test: request get number"""
        # GET_NUMBER account_0
        number, activation_id = _query_numbers(client=self.client, account=self.accounts[0], service_code='acc_0/service_0')

        self.assertIn(str(number), [n.tel_number for n in self.numbers])

        # GET_SERVICES
        all_service_maps = _query_account_service_maps(client=self.client, accounts=self.accounts)
        # account 0
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_0'], 19)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_1'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_2'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_3'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_4'], 20)
        # account 1
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_0'], 19)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_1'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_2'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_3'], 20)
        # account 2
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_0'], 19)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_1'], 20)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_2'], 20)

    def test_get_services_with_get_number_no_numbers(self):
        """test: request get number when no numbers"""
        # run uot all available activations for account0_service0
        for i in range(20):
            create_act_acc_ser(account=self.accounts[0], service=self.services[0], number=self.numbers[i])

        # GET_SERVICES
        all_service_maps = _query_account_service_maps(client=self.client, accounts=self.accounts)
        # account 0
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_0'], 0)

        # GET_NUMBER account_0
        GET_NUMBER['key'] = self.accounts[0].key
        GET_NUMBER['service'] = 'acc_0/service_0'
        response = self.client.post('/sms_hub', data=GET_NUMBER)

        self.assertEqual(response.json()['status'], 'NO_NUMBERS')

    def test_get_services_get_number_finish_activation_bad(self):
        """test: request get number and finish activation with status 4"""
        # GET_NUMBER account_0
        number, activation_id = _query_numbers(client=self.client, account=self.accounts[0],
                                               service_code='acc_0/service_0')

        # FINISH_ACTIVATION account_0
        # set tries more then allow manually
        AccountActivation.objects.filter(pk=1).update(tries=2)
        _query_finish_activation(client=self.client, account=self.accounts[0], activation_id=activation_id, status=4)

        # GET_SERVICES
        all_service_maps = _query_account_service_maps(client=self.client, accounts=self.accounts)
        # account 0
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_0'], 19)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_1'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_2'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_3'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_4'], 20)
        # account 1
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_0'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_1'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_2'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_3'], 20)
        # account 2
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_0'], 20)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_1'], 20)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_2'], 20)

    def test_get_services_get_number_finish_activation_success(self):
        """test: request get number and finish activation with status 3"""
        # GET_NUMBER account_0
        number, activation_id = _query_numbers(client=self.client, account=self.accounts[0],
                                               service_code='acc_0/service_0')

        # FINISH_ACTIVATION account_0
        response = _query_finish_activation(client=self.client, account=self.accounts[0], activation_id=activation_id, status=3)
        self.assertEqual(response.json()['status'], 'SUCCESS')
        # GET_SERVICES
        all_service_maps = _query_account_service_maps(client=self.client, accounts=self.accounts)
        # account 0
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_0'], 19)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_1'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_2'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_3'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_4'], 20)
        # account 1
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_0'], 19)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_1'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_2'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_3'], 20)
        # account 2
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_0'], 19)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_1'], 20)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_2'], 20)

    def test_not_give_same_number_to_two_accounts(self):
        """test: two account request one service concurrently"""
        # GET_NUMBER account_0
        number_0, activation_id_0 = _query_numbers(client=self.client, account=self.accounts[0],
                                                   service_code='acc_0/service_0')
        # GET_NUMBER account_1
        number_1, activation_id_1 = _query_numbers(client=self.client, account=self.accounts[1],
                                                   service_code='acc_1/service_0')

        self.assertNotEqual(number_0, number_1)

        # GET_SERVICES
        all_service_maps = _query_account_service_maps(client=self.client, accounts=self.accounts)
        # account 0
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_0'], 18)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_1'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_2'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_3'], 20)
        self.assertEqual(all_service_maps['acc_0']['acc_0/service_4'], 20)
        # account 1
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_0'], 18)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_1'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_2'], 20)
        self.assertEqual(all_service_maps['acc_1']['acc_1/service_3'], 20)
        # account 2
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_0'], 18)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_1'], 20)
        self.assertEqual(all_service_maps['acc_2']['acc_2/service_2'], 20)
