import datetime

from django.test import TestCase

from accounts.models import Account, AccountService, AccountActivation
from mtt.models import TelNumber
from sms_hub.models import Service, Activation
from tests.fixture import get_account_service, create_act_acc_ser
from utils import number_utils
from tests import fixture


# TODO: creating activations by func
from utils.number_utils import get_available_numbers_between_services


class GetAvailableNumbersTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # set up base data (manually setting data to start)
        fixture.set_test_data(cls=cls)

    def test_get_available_numbers_without_activations(self):
        """test: get all available number for account and service"""
        account, service, account_service = get_account_service(0, 0)
        numbers = number_utils._get_available_all_numbers(account_service=account_service)
        self.assertEqual(len(numbers), 20)

    def test_get_available_numbers_with_activations(self):
        """test: get available number for account and service"""
        number = TelNumber.objects.all().first()
        account, service, account_service = get_account_service(0, 0)

        activation = Activation.objects.create(account=account, service=service, tel_number=number)  # status=0
        numbers = number_utils._get_available_all_numbers(account_service=account_service)
        self.assertEqual(len(numbers), 19)

        activation.status = 3
        activation.save()
        numbers = number_utils._get_available_all_numbers(account_service=account_service)
        self.assertEqual(len(numbers), 19)

        activation.status = 1
        activation.save()
        numbers = number_utils._get_available_all_numbers(account_service=account_service)
        self.assertEqual(len(numbers), 19)

        # still no AccountActivation
        activation.status = 4
        activation.save()
        numbers = number_utils._get_available_all_numbers(account_service=account_service)
        self.assertEqual(len(numbers), 20)

    def test_get_retry_numbers(self):
        number = TelNumber.objects.all().first()
        account, service, account_service = get_account_service(0, 0)

        _, account_activation = create_act_acc_ser(account=account, service=service, number=number,
                                                   status=Activation.STATUS.error, tries=3)

        numbers = number_utils._get_available_all_numbers(account_service=account_service)
        self.assertEqual(len(numbers), 19)

        account_activation.tries = 3
        account_activation.save()
        numbers = number_utils._get_available_all_numbers(account_service=account_service)
        self.assertEqual(len(numbers), 19)

        account_activation.tries = 1
        account_activation.last_try_datetime = datetime.datetime.now()
        account_activation.save()
        numbers = number_utils._get_available_all_numbers(account_service=account_service)
        self.assertEqual(len(numbers), 19)

        account_activation.tries = 1
        # fake: last try was 5 sec ago
        account_activation.last_try_datetime = datetime.datetime.now() - datetime.timedelta(seconds=6)
        account_activation.save()
        numbers = number_utils._get_available_all_numbers(account_service=account_service)
        self.assertEqual(len(numbers), 20)

    def test_get_available_numbers_between_services(self):
        """test: get available number for several services"""
        all_numbers = TelNumber.objects.all()
        for i in range(3):
            create_act_acc_ser(account=self.accounts[0], service=self.services[i], number=self.numbers[i])
        for i in range(2):
            create_act_acc_ser(account=self.accounts[1], service=self.services[i], number=self.numbers[3+i],
                               tries=4, status=4)

        account_services = AccountService.objects.filter(account=self.accounts[0])
        exclude_accounts = Account.objects.filter(pk=self.accounts[1].id)
        numbers = get_available_numbers_between_services(account_services=account_services,
                                                         tel_numbers=all_numbers,
                                                         exclude_accounts=exclude_accounts)
        self.assertEqual(len(numbers), 20)
        self.assertIn(all_numbers[0], numbers)
        self.assertIn(all_numbers[1], numbers)
        self.assertIn(all_numbers[2], numbers)
        self.assertIn(all_numbers[3], numbers)
        self.assertIn(all_numbers[4], numbers)

        account_services = AccountService.objects.filter(account=self.accounts[1])
        exclude_accounts = Account.objects.filter(pk=self.accounts[0].id)
        numbers = get_available_numbers_between_services(account_services=account_services,
                                                         tel_numbers=all_numbers,
                                                         exclude_accounts=exclude_accounts)
        self.assertEqual(len(numbers), 17)
        self.assertNotIn(all_numbers[0], numbers)
        self.assertNotIn(all_numbers[1], numbers)
        self.assertNotIn(all_numbers[2], numbers)
        self.assertIn(all_numbers[3], numbers)
        self.assertIn(all_numbers[4], numbers)