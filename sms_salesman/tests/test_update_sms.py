from django.test import TestCase

from accounts.models import AccountSms
from mtt.models import Sms
from sms_hub.models import Activation
from tests.fixture import set_test_data, create_act_acc_ser, gen_message_data_from_mtt
from utils.activation_utils import get_active_activations
from utils.sms_utils import create_sms_from_mtt


class UpdateSmsTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        # set up base data (manually setting data to start)
        set_test_data(cls=cls)

    def test_active_tel_numbers(self):
        """test: all active activation"""
        for i in range(2):
            create_act_acc_ser(account=self.accounts[0], service=self.services[0], number=self.numbers[i],
                           status=Activation.STATUS.active)
        for i in range(3):
            create_act_acc_ser(account=self.accounts[2], service=self.services[2], number=self.numbers[i],
                           status=Activation.STATUS.active)
        active_activations = get_active_activations()
        self.assertEqual(len(active_activations), 3)

    def test_create_sms_from_mtt(self):
        """test: create sms and account_sms with data from_mtt"""
        # create activations with different services for different accounts with some cross numbers
        for i in range(5):
            create_act_acc_ser(account=self.accounts[0], service=self.services[0], number=self.numbers[i],
                               status=Activation.STATUS.active)
        for i in range(2, 7):
            create_act_acc_ser(account=self.accounts[1], service=self.services[1], number=self.numbers[i],
                               status=Activation.STATUS.active)
        for i in range(4, 9):
            create_act_acc_ser(account=self.accounts[2], service=self.services[2], number=self.numbers[i],
                               status=Activation.STATUS.active)

        # get messages_data
        for i in range(9):
            # create one messages for every number and create smses with it
            message_data = gen_message_data_from_mtt(1, tel_number=self.numbers[i])[0]
            create_sms_from_mtt(tel_number=self.numbers[i], mtt_data=message_data)

        all_sms = Sms.objects.all()
        self.assertEqual(len(all_sms), 9)

        self.assertEqual(AccountSms.objects.filter(account=self.accounts[0]).count(), 5)
        self.assertEqual(AccountSms.objects.filter(account=self.accounts[1]).count(), 5)
        self.assertEqual(AccountSms.objects.filter(account=self.accounts[2]).count(), 5)

        self.assertEqual(AccountSms.objects.filter(sms=all_sms[0]).count(), 1)
        self.assertEqual(AccountSms.objects.filter(sms=all_sms[1]).count(), 1)
        self.assertEqual(AccountSms.objects.filter(sms=all_sms[2]).count(), 2)
        self.assertEqual(AccountSms.objects.filter(sms=all_sms[3]).count(), 2)
        self.assertEqual(AccountSms.objects.filter(sms=all_sms[4]).count(), 3)
        self.assertEqual(AccountSms.objects.filter(sms=all_sms[5]).count(), 2)
        self.assertEqual(AccountSms.objects.filter(sms=all_sms[6]).count(), 2)
        self.assertEqual(AccountSms.objects.filter(sms=all_sms[7]).count(), 1)
        self.assertEqual(AccountSms.objects.filter(sms=all_sms[8]).count(), 1)
