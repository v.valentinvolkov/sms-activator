from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model

from accounts.models import Account, AccountService, AccountActivation, AccountSms

admin.site.register(get_user_model(), UserAdmin)


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(AccountActivation)
class AccountActivationAdmin(admin.ModelAdmin):
    list_display = ('account', 'activation', 'tries', 'last_try_datetime')


@admin.register(AccountService)
class AccountServiceAdmin(admin.ModelAdmin):
    list_display = ('account', 'service', 'allowed_tries', 'try_interval', 'priority', 'limit', 'is_active')


@admin.register(AccountSms)
class AccountSmsAdmin(admin.ModelAdmin):
    list_display = ('account', 'sms', 'is_success')
