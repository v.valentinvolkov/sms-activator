from django.core.management import BaseCommand

from accounts.models import Account


class Command(BaseCommand):
    help = 'Create accounts'

    def handle(self, *args, **options):
        Account.objects.get_or_create(
            name='smshub',
            key='1762U88aa199db7c0f5d5c161ffa4d16704ae',
            sms_endpoint='https://pencil.unerio.com:2087/agent/api/sms',
            operator='mtt_virtual'
        )
        Account.objects.get_or_create(
            name='onlinesim',
            key='9cf6d8b693613a058ca0b86dd4724b49',
            sms_endpoint='http://onlinesim.ru/api/sms_in_getaways/frissk',
            operator='any'
        )
        Account.objects.get_or_create(
            name='onlinesim_v2',
            key='key onlinesim_v2',
            sms_endpoint='endpoint onlinesim_v2',
            operator='any'
        )