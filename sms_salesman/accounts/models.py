from django.db import models
from django.contrib.auth.models import AbstractUser
from model_utils import fields


class User(AbstractUser):
    pass


class Account(models.Model):
    name = models.CharField('Название', max_length=25, unique=True)
    key = models.CharField('Ключ', max_length=255, unique=True)
    sms_endpoint = models.CharField('Ссылка для смс', max_length=255, unique=True)
    operator = models.CharField('Название оператора', max_length=50, default='any')

    def __str__(self):
        return self.name


class AccountService(models.Model):
    service = models.ForeignKey('sms_hub.Service', on_delete=models.CASCADE, related_name='accounts')
    account = models.ForeignKey('Account', on_delete=models.CASCADE, related_name='service')
    # Limit by success activations only
    limit = models.IntegerField('Макс. продаж', default=0)
    priority = models.IntegerField('Приоритет')
    service_code = models.CharField('Обознач.', max_length=20, blank=False)
    allowed_tries = models.IntegerField('Кол. попыток', default=1)
    try_interval = models.DurationField('Время между попытками', default='04:00:10')
    # is_active = False return 0 in GET_SERVICES, but limit will still affect in priority filters
    is_active = models.BooleanField('Активный', default=True)

    constraints = [
        models.UniqueConstraint(fields=['service', 'account'], name='unique_account_setting'),
        models.UniqueConstraint(fields=['service', 'priority'], name='unique_service_priority'),
        models.UniqueConstraint(fields=['account', 'service_code'], name='unique_service_priority'),
    ]

    def __str__(self):
        return f'{self.account}/{self.service}'


class AccountActivation(models.Model):
    activation = models.ForeignKey('sms_hub.Activation',
                                   on_delete=models.CASCADE,
                                   related_name='accounts',
                                   related_query_name='account')
    account = models.ForeignKey('Account',
                                 on_delete=models.CASCADE,
                                 related_name='activations',
                                 related_query_name='activation')

    tries = models.IntegerField('Попытки', default=0)
    last_try_datetime = fields.MonitorField('Время последней попытки', monitor='tries')

    constraints = [
        models.UniqueConstraint(fields=['activation', 'account'], name='unique_account_activation')
    ]

    def __str__(self):
        return f'{self.account.name}/{self.activation.service.name}'


class AccountSms(models.Model):
    account = models.ForeignKey('Account', on_delete=models.CASCADE, related_name='sms')
    sms = models.ForeignKey('mtt.Sms', on_delete=models.CASCADE, related_name='account')
    is_success = models.BooleanField('Смс доставлена', default=False)

    constraints = [
        models.UniqueConstraint(fields=['sms', 'account'], name='unique_platform_activation')
    ]

    def __str__(self):
        return f'{self.account.name}/sms {self.sms.pk}'