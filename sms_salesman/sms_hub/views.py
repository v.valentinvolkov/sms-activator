import logging

from django.conf import settings
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from accounts.models import Account, AccountService
from mtt.models import TelNumber
from sms_hub.models import Activation
from utils.activation_utils import try_init_activation, finish_activation
from utils.number_utils import get_service_map, get_number
from utils.service_utils import get_account_service_by_code

logger = logging.getLogger('smshub')


@api_view(['POST'])
@permission_classes([AllowAny])
def sms_hub(request):
    action: str = request.data['action']
    key: str = request.data['key']

    try:
        try:
            account = Account.objects.get(key=key)
        except Account.DoesNotExist:
            logger.warning(f'Unknown account key: {key}')
            return Response(
                data={'status': 'ERROR', 'error': f'Unknown account key: {key}'},
                status=400,
                content_type='application/json; charset=utf-8'
            )
        if action == 'GET_SERVICES':
            try:
                logger.info(f'GET_SERVICES {account.name} request: {request.data}')
                log_extra_info = ''

                # for max active sms check
                all_active_sms_count = settings.ZERO_AVAILABLE_NUMBERS

                # check zero returning
                if settings.ZERO_AVAILABLE_NUMBERS:
                    service_map = get_service_map(account=account, zero_available_numbers=True)
                    log_extra_info += f'zero available numbers: {settings.ZERO_AVAILABLE_NUMBERS}'
                elif all_active_sms_count > settings.MAX_ACTIVE_SMS_POOL:
                    service_map = get_service_map(account=account, zero_available_numbers=True)
                    log_extra_info += f'active sms: {all_active_sms_count} > {settings.MAX_ACTIVE_SMS_POOL}'
                else:
                    service_map = get_service_map(account=account, zero_available_numbers=False)

                response = {
                    'status': 'SUCCESS',
                    "countryList": [
                        {
                            "country": "russia",
                            "operatorMap": {
                                account.operator: service_map
                            }
                        }
                    ]
                }

                logger.info(f'GET_SERVICES {account.name} response; {log_extra_info}')
                return Response(response, content_type='application/json; charset=utf-8')
            except Exception:
                response = {
                    'status': 'SUCCESS',
                    "countryList": [
                        {
                            "country": "russia",
                            "operatorMap": {
                                account.operator: {}
                            }
                        }
                    ]
                }
                return Response(response, content_type='application/json; charset=utf-8')
        elif action == 'GET_NUMBER':
            try:
                logger.info(f'GET_NUMBER {account.name} request: {request.data}')
                service_code: str = request.data.get('service')
                exclude_prefixes: list[str] = request.data.get('exceptionPhoneSet', [])

                account_service = get_account_service_by_code(account=account, service_code=service_code)
                tel_numbers = get_number(account_service=account_service, exclude_prefixes=exclude_prefixes)
                if not tel_numbers.exists():
                    logger.warning(f'GET_NUMBER {account.name} response: {service_code} - no numbers')
                    return Response({"status": 'NO_NUMBERS'}, content_type='application/json; charset=utf-8')
                activation = try_init_activation(account_service=account_service, tel_numbers=tel_numbers)
                if activation is None:
                    logger.warning(f'GET_NUMBER {account.name} response: {service_code} - concurrent error')
                    return Response({"status": 'ERROR', 'error': 'Concurrent error - too many request/sec'},
                                    content_type='application/json; charset=utf-8')
                response = {
                    "number": int(activation.tel_number.tel_number),
                    "activationId": activation.pk,
                    "status": 'SUCCESS'
                }

                logger.info(f'GET_NUMBER {account.name} response: {service_code}/{activation.tel_number.tel_number}')
                return Response(response, content_type='application/json; charset=utf-8')
            except AccountService.DoesNotExist:
                logger.warning(f'GET_NUMBER {account.name} response: {service_code} - unknown service')
                return Response({"status": 'ERROR', 'error': f'Unknown service: {service_code}'},
                                content_type='application/json; charset=utf-8')
        elif action == 'FINISH_ACTIVATION':
            try:
                logger.info(f'FINISH_ACTIVATION {account.name} request: {request.data}')
                activation_id: int = int(request.data.get('activationId'))
                status: int = int(request.data.get('status'))

                activation, account_activation = finish_activation(
                    account=account,
                    activation_id=activation_id,
                    status=status
                )
                logger.info(f'FINISH_ACTIVATION {account.name} response: '
                            f'{activation.tel_number.tel_number}/{activation.service.name}')
                return Response({'status': 'SUCCESS'}, content_type='application/json; charset=utf-8')
            except Activation.DoesNotExist:
                logger.warning(f'FINISH_ACTIVATION {account.name} response: '
                               f'{activation_id} - activation does not exist')
                return Response({'status': 'SUCCESS'}, content_type='application/json; charset=utf-8')
    except Exception as e:
        logger.exception(str(e))
        return Response(data={'status': 'ERROR', 'error': 'Internal error'},
                        status=400,
                        content_type='application/json; charset=utf-8')
