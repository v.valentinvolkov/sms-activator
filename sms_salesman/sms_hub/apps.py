from django.apps import AppConfig


class SmsHubConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sms_hub'
