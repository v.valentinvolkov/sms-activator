import logging
import sys
import time

import requests
from django.core.management import BaseCommand
from django.db.models import QuerySet

from accounts.models import Account, AccountService
from utils.activation_utils import init_activation
from utils.number_utils import get_available_numbers_between_services

logger = logging.getLogger('send_numbers_v2')


def get_numbers_for_online_v2(account: Account,
                              limit: int = None,
                              exclude_accounts: QuerySet[Account] = None,
                              collect_only: bool = False) -> list[str]:
    """ Get numbers that available for all active AccountService and make them active. """
    # all active services for account
    account_services = AccountService.objects.filter(account=account, is_active=True)
    # all available numbers between all account_services
    tel_numbers = get_available_numbers_between_services(account_services=account_services,
                                                         exclude_accounts=exclude_accounts)
    # set limit
    if limit is not None:
        tel_numbers = tel_numbers[:limit]
    # init activation with every number for each service
    str_numbers_to_send = []
    for i, tel_number in enumerate(tel_numbers):
        print(f'tel number {i}')
        try:
            if not collect_only:
                for account_service in account_services:
                    init_activation(account_service=account_service, tel_number=tel_number)
        except Exception as e:
            print(e)
            continue
        else:
            print(f'add {tel_number.tel_number}')
            str_numbers_to_send.append(tel_number.tel_number)
    return str_numbers_to_send


def send_numbers(url: str, numbers: list[str]):
    """ Make "sendNumbersOnline" request with numbers. """
    data = {
        "numbers": [
            {"number": number, "country_id": 7} for number in numbers
        ]
    }
    response = requests.post(url=url, json=data, headers={"Authorization": "9cf6d8b693613a058ca0b86dd4724b49"})
    if response.status_code == 200:
        logger.info(f'Sent {len(numbers)} numbers v2')
    else:
        logger.warning(f'Failed sending of numbers v2; response: {response.json()}')


class Command(BaseCommand):
    help = 'Send list of numbers to online sim v2'

    ONLINESIM_ACCOUNT_V2_NAME = 'onlinesim_v2'
    EXCLUDE_ACCOUNTS_NAMES = ['smshub']
    URL_SEND_NUMER = 'https://simsale.net/api/resellers/sendNumbersOnline'

    def add_arguments(self, parser):
        parser.add_argument('--interval', type=int, default=300,
                            help='Interval between calling (sec)')

        parser.add_argument('--limit', type=int,
                            help='Limit of numbers')

        parser.add_argument('--collect-only', action='store_true',
                            help='Limit of numbers')

    def handle(self, *args, **options):
        account = Account.objects.get(name=self.ONLINESIM_ACCOUNT_V2_NAME)
        exclude_accounts = Account.objects.filter(name__in=self.EXCLUDE_ACCOUNTS_NAMES)
        numbers = get_numbers_for_online_v2(account=account,
                                            limit=options['limit'],
                                            exclude_accounts=exclude_accounts,
                                            collect_only=options.get('collect_only'))
        if options.get('collect_only'):
            print(f'Collect only available numbers count: {len(numbers)}')
            print(f'exclude_accounts: {list(exclude_accounts.values_list("name", flat=True))}')
            sys.exit('Collect-only - no running cycle, no new activations')
        while True:
            try:
                logger.info(f'Available numbers: {len(numbers)}')
                send_numbers(url=self.URL_SEND_NUMER, numbers=numbers)
            except Exception as e:
                logger.error(f'Error while sending numbers: {e}')
            time.sleep(options['interval'])
