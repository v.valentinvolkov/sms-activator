from django.core.management.base import BaseCommand
from django.db import IntegrityError
import csv
import sys

from accounts.models import Account, AccountActivation, AccountService
from mtt.models import TelNumber
from sms_hub.models import Service, Activation


class Command(BaseCommand):
    help = 'Load activations from scv -\n' \
           '"service; tel_number; current_account; status; account_0; account_1"\n' \
           '"vk; 79998887766; smshub_01; 3; 1; 4"' \
           '"account_*": tries'

    def add_arguments(self, parser):
        parser.add_argument('-f', type=str, help='scv file with activations', required=True)

    def handle(self, *args, **options):
        csv.register_dialect('dialect', delimiter=';')
        filename = options.get('f')
        looked_rows = 0
        new_activations = 0
        update_activations = 0
        new_a_activations = 0
        update_a_activations = 0
        errors = 0
        with open(filename, newline='') as f:
            reader = csv.DictReader(f, dialect='dialect')
            try:
                for row in reader:
                    print(row)
                    looked_rows += 1
                    service = Service.objects.get(name=row['service'])
                    tel_number = TelNumber.objects.get(tel_number=row['tel_number'])
                    current_account = Account.objects.get(name=row['current_account'])
                    status = int(row['status'])
                    # TODO: not valid status?
                    # create or update activation
                    activation, created = Activation.objects.update_or_create(service=service,
                                                                              tel_number=tel_number,
                                                                              defaults={
                                                                                  'status': status,
                                                                                  'current_account': current_account
                                                                              })


                    if created:
                        new_activations += 1
                    else:
                        update_activations += 1
                    # create or update account_activations
                    for account_name, tries in list(row.items())[4:]:
                        if tries == '':
                            continue
                        # create or update account activation
                        tries = int(tries)
                        account = Account.objects.get(name=account_name)
                        account_activation, created = AccountActivation.objects.update_or_create(activation=activation,
                                                                                                 account=account,
                                                                                                 defaults={
                                                                                                     'tries': tries,
                                                                                                 })

                        print(f'    - {account_activation} {"create" if created else "updated"}')
                        if created:
                            new_a_activations += 1
                        else:
                            update_a_activations += 1
            except csv.Error as e:
                sys.exit('file {}, line {}: {}'.format(filename, reader.line_num, e))
            except Exception as e:
                print(e)
                errors += 1
        print(f'looked_rows: {looked_rows}\n'
              f'new_activations: {new_activations}\n'
              f'update_activations: {update_activations}\n'
              f'new_a_activations: {new_a_activations}\n'
              f'update_a_activations: {update_a_activations}\n'
              f'errors: {errors}\n')
