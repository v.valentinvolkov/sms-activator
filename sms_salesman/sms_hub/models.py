import datetime

import django
from django.db import models
from model_utils import Choices, fields


class Service(models.Model):
    MODES = Choices((0, 'full', 'Полностью'),
                    (1, 'limit', 'Лимит'),
                    (2, 'priority', 'Приоритет'),
                    (3, 'limit_priority', 'Лимит-приоритет'))
    name = models.CharField('Название', max_length=50, unique=True)
    mode = models.IntegerField('Распределение', choices=MODES, default=MODES.full)
    is_active = models.BooleanField('Активный', default=True)

    def __str__(self):
        return self.name


class Activation(models.Model):
    STATUS = Choices((0, 'active', 'Активация'),
                     (1, 'bad', 'Был использован'),
                     (3, 'success', 'Проданно'),
                     (4, 'error', 'Ошибка'))

    service = models.ForeignKey(Service, on_delete=models.PROTECT)
    tel_number = models.ForeignKey('mtt.TelNumber',
                                   on_delete=models.PROTECT,
                                   related_name='activations',
                                   related_query_name='activation')
    status = models.IntegerField('Статус активации', choices=STATUS, default=STATUS.active)
    # account activation that have this activation now (for "send_sms")
    current_account = models.ForeignKey('accounts.Account',
                                        on_delete=models.SET_NULL, null=True,
                                        related_name='+')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['service', 'tel_number'], name='unique_activation')
        ]
        indexes = [
            models.Index(fields=['tel_number'], name='tel_number'),
        ]

    def __str__(self):
        return f'{self.service.name}/{self.tel_number.tel_number}'