from django.urls import path

from . import views

urlpatterns = [
    path('sms_hub', views.sms_hub)
]