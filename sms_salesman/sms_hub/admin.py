from django.contrib import admin
from .models import Service, Activation


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'mode', 'is_active')


@admin.register(Activation)
class ActivationAdmin(admin.ModelAdmin):
    list_display = ('service', 'tel_number', 'status')
